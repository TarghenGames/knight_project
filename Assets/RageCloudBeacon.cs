﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RageCloudBeacon : MonoBehaviour {

    [HideInInspector]
    public GameObject current_target;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            current_target = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            current_target = null;
        }
    }
}

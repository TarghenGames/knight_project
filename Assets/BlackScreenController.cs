﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackScreenController : MonoBehaviour {

    public enum BlackScreenEnum
    {
        Menu,
        Death
    }

    public static BlackScreenController black_screen;

    GameObject black_screen_go;
    GameObject text_go;
    GameObject menu_go;
    bool show_black;
    bool hide;
    Color color;

    List<string> descriptor;

    public BlackScreenEnum to_show;

    // Use this for initialization
    void Start () {
        descriptor = new List<string>();
        descriptor.Add("dissapeared into the void.");
        descriptor.Add("sucks");
        descriptor.Add("did not saved enough");
        descriptor.Add("run out of hearts");
        descriptor.Add("is to young to die");

        show_black = false;
        hide = false;
        black_screen_go = transform.GetChild(0).gameObject;
        black_screen_go.SetActive(false);
        text_go = transform.GetChild(0).GetChild(0).gameObject;
        text_go.SetActive(false);

        menu_go = transform.GetChild(0).GetChild(1).gameObject;
        menu_go.SetActive(false);

        color = black_screen_go.GetComponent<Image>().color;
        color.a = 0f;
        black_screen_go.GetComponent<Image>().color = color;

        black_screen = this;
    }

    public void SetType(BlackScreenEnum show)
    {
        if (to_show == BlackScreenEnum.Death) { return; }
        to_show = show;
    }

    public void ShowBlackScreen()
    {
        black_screen_go.SetActive(true);
        show_black = true;
    }

    public void HideBlackScreen()
    {
        menu_go.SetActive(false);
        text_go.SetActive(false);
        hide = true;
    }

    public void SetPlayerName(string s)
    {
        player_name = s;
    }

    string player_name;

    private void Update()
    {
        if (show_black == true)//*TODO* i don't like how this is working, i should redo it
        {
            color = black_screen_go.GetComponent<Image>().color;
            color.a += 0.02f;
            black_screen_go.GetComponent<Image>().color = color;
            if (color.a >= 1f)
            {
                switch (to_show)
                {
                    case BlackScreenEnum.Death:
                        text_go.SetActive(true);
                        text_go.GetComponent<Text>().text = player_name + " " + descriptor[UnityEngine.Random.Range(0, descriptor.Count)];
                        show_black = false;
                        break;
                    case BlackScreenEnum.Menu:
                        menu_go.SetActive(true);
                        show_black = false;
                        break;
                }

            }
        }
        else {
            if (hide == true)
            {
                color = black_screen_go.GetComponent<Image>().color;
                if (color.a <= 0f) { hide = false; show_black = false; return; }
                color.a -= 0.02f;
                black_screen_go.GetComponent<Image>().color = color;
            }
        }
    }
}

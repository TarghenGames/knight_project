﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCharacter : MonoBehaviour {
    string chracter_name = "Jim";
    string title = "Knight";
    int max_hp = 6;
    int hp = 6;
    public float blinking_cd = 1f;

    public float control_cut_off_time = 1f;
    int score;
    public int SCORE {
        get { return score; }
        set {
            score = value;
            UpdateScore();
        }
    }

    public bool IS_ALIVE
    {
        get
        {
            if (hp <= 0)
            {
                return false;
            }
            return true;
        }
    }

    int coin;
    public int COIN
    {
        get { return coin; }
        set
        {
            coin = value;
            UpdateCoin();
        }
    }

    bool ignore_damage = false;

    [HideInInspector]
    public float blinking = 0f;
    SpriteRenderer sprite_renderer;
    Text score_text;//done to decrease amount of codes in game
    Text coin_text;

    Animator animator;

    public int HP {
        get { return hp; }
        set
        {
            hp = value;
            if (hp > max_hp) { hp = max_hp; }
            if (hp <= 0) { Defeated(); }
            hp_bar.HPBarUpdate(hp);
        }
    }

    HPBarController hp_bar;

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        sprite_renderer = gameObject.GetComponent<SpriteRenderer>();
        hp_bar = GameObject.Find("HPBAR").GetComponent<HPBarController>();
        hp_bar.Initialize(max_hp, hp);
        score_text = GameObject.Find("ScorePanel").GetComponent<Text>();
        coin_text = GameObject.Find("CoinPanel").GetComponent<Text>();
        LoadCharacterInformation();
    }

    private void LoadCharacterInformation()
    {
        Container cnt = GameObject.Find("Container").GetComponent<Container>();
        if (cnt == null) { return; }
        chracter_name = cnt.knight_name;
    }

    private void Update()
    {
        if (blinking > 0f)
        {
            animator.SetFloat("Damage_Timer", blinking - control_cut_off_time);
            blinking -= Time.deltaTime;
            if (blinking <= 0f)
            {
                sprite_renderer.color = new Color(1f, 1f, 1f, 1f);
                Physics2D.IgnoreLayerCollision(transform.gameObject.layer, LayerMask.NameToLayer("Enemy_Projectile"), false);
                ignore_damage = false;
            }
        }
    }

    public void NamingThePlayer(string _name, string _title)
    {
        chracter_name = _name;
        title = _title;
    }

    public void Defeated()
    {
        gameObject.GetComponent<PlayerControllScript>().enabled = false;
        BlackScreenController.black_screen.ShowBlackScreen();
        BlackScreenController.black_screen.SetPlayerName(chracter_name);
        BlackScreenController.black_screen.SetType(BlackScreenController.BlackScreenEnum.Death);
        gameObject.layer = LayerMask.NameToLayer("Enemy_Ignore_Terrain");//to make character to fly through all objects
        Camera.main.GetComponent<DefeatedController>().enabled = true;
    }

    public void Heal(int amount)
    {
        HP += amount;
    }

    public void TakeDamage(int damage, bool right)
    {
        if (blinking <= 0f)
        {
            HP -= damage;
            float f = 1;
            if (right) { f = 1; f = -1; }
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * f * 50f + Vector2.up * 80f);
            sprite_renderer.color = new Color(1f, 1f, 1f, 0.5f);
            blinking = blinking_cd;
            animator.Play("Damaged");
            animator.SetFloat("Damage_Timer", blinking - control_cut_off_time);
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy_Projectile"), true);
            ignore_damage = true;
        }
    }

    void UpdateScore()
    {
        score_text.text = "SCORE:" + score.ToString();
    }

    void UpdateCoin()
    {
        if (coin_text == null) { return; }
        coin_text.text = coin.ToString();
    }

    internal void AddCoin()
    {
        COIN += 1;
    }
}

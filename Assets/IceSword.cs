﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceSword : MonoBehaviour {
    public GameObject ice_block;
    float ttl = 0.5f;
    SpriteRenderer sprite_renderer;

    private void Start()
    {
        sprite_renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        ttl -= Time.deltaTime;
        if (ttl < 0.2f)
        {
            sprite_renderer.color = new Color(1f, 1f, 1f, 0.5f);
        }
        if (ttl < 0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    { 
        Instantiate(ice_block, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

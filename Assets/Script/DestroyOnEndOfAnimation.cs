﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnEndOfAnimation : MonoBehaviour {

    public void DestroyMe() {
        Destroy(gameObject);
    }
}

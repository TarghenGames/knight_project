﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cape_Controller : MonoBehaviour {

    Animator animator;
    bool crouched_down;
    PlayerControllScript pcs;

    private void Start()
    {
        animator = transform.GetChild(1).GetComponent<Animator>();
        pcs = gameObject.GetComponent<PlayerControllScript>();
    }

    public void Run()
    {
        if (animator.gameObject.activeInHierarchy == false) { return; }
        animator.SetBool("Runing", true);
    }

    public void Attack()
    {
        if (animator.gameObject.activeInHierarchy == false) { return; }
        animator.SetBool("Attack", true);
    }

    public void CrouchDown()
    {
        if (animator.gameObject.activeInHierarchy == false) { return; }

        crouched_down = true;
    }

    public void StandUp()
    {
        if (animator.gameObject.activeInHierarchy == false) { return; }
        crouched_down = false;
    }


    private void Update()
    {
        //*TODO* optimize
        if (pcs.attacking == true) { return; }
        if (crouched_down)
        {
            animator.Play("CapeCrouchIdle");
            animator.SetBool("CrouchDown", crouched_down);
        }
        else
        {
            animator.SetBool("CrouchDown", crouched_down);
        }
    }

    public void Normalize()
    {
        if (animator.gameObject.activeInHierarchy == false) { return; }
        animator.SetBool("Attack", false);
        animator.SetBool("Runing", false);
    }

    public void Hide()
    {
        animator.gameObject.SetActive(false);
    }

    public void Show()
    {
        animator.gameObject.SetActive(true);
    }
}

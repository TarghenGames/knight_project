﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour {
    public GameObject follow;

    GameObject limit_left_bottom;
    GameObject limit_right_top;

    private void Start()
    {
        limit_left_bottom = GameObject.Find("Limit_Left_Bottom");
        limit_right_top = GameObject.Find("Limit_Right_Top");
    }

    // Update is called once per frame
    void Update () {
        transform.position = follow.transform.position - Vector3.forward * 10f;
        //height limit
        if (limit_left_bottom.transform.position.x > transform.position.x)
        {
            transform.position = new Vector3(limit_left_bottom.transform.position.x, transform.position.y, -10f);
        }
        if (limit_right_top.transform.position.x < transform.position.x)
        {
            transform.position = new Vector3(limit_right_top.transform.position.x, transform.position.y, -10f);
        }

        //width limit
        if (limit_left_bottom.transform.position.y > transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, limit_left_bottom.transform.position.y, -10f);
        }
        if (limit_right_top.transform.position.y < transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, limit_right_top.transform.position.y, -10f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Rotation {
    No,
    Right90,
    Normalize,
    Hide,
    HideIn50MS
} 

public class AnimationSynchronization : MonoBehaviour {
    Transform weapon_transform;
    PlayerControllScript pcs;
    List<Vector3> standing_attack_weapon_position;
    List<Rotation> standing_attack_rotation;

    List<Vector3> crouched_attack_weapon_position;
    List<Rotation> crouched_attack_rotation;

    // Use this for initialization
	void Start () {
        weapon_transform = transform.GetChild(0);
        weapon_transform.gameObject.SetActive(false);
        pcs = gameObject.GetComponent<PlayerControllScript>();


        standing_attack_weapon_position = new List<Vector3>();
        standing_attack_weapon_position.Add(new Vector3(-0.129f, 0.174f));
        standing_attack_weapon_position.Add(new Vector3(0.189f, -0.022f));
        standing_attack_weapon_position.Add(new Vector3(0.189f, -0.022f));
        standing_attack_rotation = new List<Rotation>();
        standing_attack_rotation.Add(Rotation.Normalize);
        standing_attack_rotation.Add(Rotation.Right90);
        standing_attack_rotation.Add(Rotation.HideIn50MS);


        crouched_attack_weapon_position = new List<Vector3>();
        crouched_attack_weapon_position.Add(new Vector3(-0.103f, 0.109f));
        crouched_attack_weapon_position.Add(new Vector3(0.189f, -0.11f));
        crouched_attack_weapon_position.Add(new Vector3(0.189f, -0.11f));

        crouched_attack_rotation = new List<Rotation>();
        crouched_attack_rotation.Add(Rotation.Normalize);
        crouched_attack_rotation.Add(Rotation.Right90);
        crouched_attack_rotation.Add(Rotation.HideIn50MS);
    }

    public void AnimationSynch(int frame)
    {
        if (pcs.crouched == true)
        {
            weapon_transform.localPosition = crouched_attack_weapon_position[frame];
            switch (crouched_attack_rotation[frame])
            {
                case Rotation.Normalize:
                    weapon_transform.gameObject.SetActive(true);
                    weapon_transform.localRotation = new Quaternion();
                    break;
                case Rotation.Right90:
                    weapon_transform.Rotate(-Vector3.forward * 90);
                    break;
                case Rotation.Hide:
                    weapon_transform.gameObject.SetActive(false);
                    break;
                case Rotation.HideIn50MS:
                    StartCoroutine(Hide());
                    break;
            }
        }
        else {
            weapon_transform.localPosition = standing_attack_weapon_position[frame];
            switch (standing_attack_rotation[frame])
            {
                case Rotation.Normalize:
                    weapon_transform.gameObject.SetActive(true);
                    weapon_transform.localRotation = new Quaternion();
                    break;
                case Rotation.Right90:
                    weapon_transform.Rotate(-Vector3.forward * 90);
                    break;
                case Rotation.Hide:
                    weapon_transform.gameObject.SetActive(false);
                    break;
                case Rotation.HideIn50MS:
                    StartCoroutine(Hide());
                    break;
            }
        }

    }

    IEnumerator Hide()
    {
        yield return new WaitForSeconds(.05f);
        weapon_transform.gameObject.SetActive(false);
    }
}

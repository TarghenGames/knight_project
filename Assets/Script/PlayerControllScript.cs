﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllScript : MonoBehaviour {
    Rigidbody2D rgbd2d;

    public GameObject landed_trigger;

    public GameObject start_trigger;
    public GameObject start_trigger_2;

    float distance;

    Animator animator;

    Cape_Controller cape_animator;
    Dust_Controller dust_animator;
    Animator double_jump_animation;
    PlayerWeaponController attack_controller;
    PlayerCharacter character;

    bool onLader = false;
    bool isClimbable = false;
    bool double_jump_available = true;

    float default_gravity_scale;

    private void Start()
    {
        character = gameObject.GetComponent<PlayerCharacter>();
        rgbd2d = gameObject.GetComponent<Rigidbody2D>();
        distance = Vector2.Distance(start_trigger.transform.position, landed_trigger.transform.position);
        animator = gameObject.GetComponent<Animator>();
        cape_animator = gameObject.GetComponent<Cape_Controller>();
        dust_animator = transform.GetChild(0).GetComponent<Dust_Controller>();
        attack_controller = transform.GetChild(2).GetComponent<PlayerWeaponController>();
        double_jump_animation = transform.GetChild(6).GetComponent<Animator>();

        default_gravity_scale = rgbd2d.gravityScale;
    }

    public float jump_force = 110f;
    public float jump_force_charger = 210f;
    public float move_speed = 1.1f;
    float pause_between_jumps = 0f;
    public float air_control_rate = 0.7f;

    public bool crouched;
    bool fixed_crouched = false;
    bool jumping = false;

    float jump_charger = 0;

    // Update is called once per frame
    void Update() {
        CheckMovingPlatform();

        cape_animator.Normalize();//*TODO* optimize this, by removing it

        Jump();
        CrouchControl();
        Movement();

        if (Input.GetKey(KeyCode.W) && character.blinking <= character.control_cut_off_time)
        {
            ClimbUpLadder();
        }
        if (Input.GetKey(KeyCode.S) && character.blinking <= character.control_cut_off_time)
        {
            ClimbDownLadder();
        }
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            animator.SetBool("Climbing", false);
        }
        if (onLader == true && character.blinking <= character.control_cut_off_time)//left right movement
        {
            if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
            {
                rgbd2d.velocity = new Vector2(rgbd2d.velocity.x, 0f);
            }
            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
            {
                rgbd2d.velocity = new Vector2(0f, rgbd2d.velocity.y);
            }
        }

        if ((Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) && CheckLanded() == true)
        {
            rgbd2d.velocity = new Vector2(0, rgbd2d.velocity.y);
        }

        //attack
        if (Input.GetKeyDown(KeyCode.F) && character.blinking <= character.control_cut_off_time)
        {
            Attack();
        }
    }

    void DropDownThroughPlatform()
    {
        if (landed_on_left.transform.tag != "DropDownPlatform" || landed_on_right.transform.tag != "DropDownPlatform") { return; }
        //Debug.Log(Physics2D.GetIgnoreCollision(gameObject.GetComponent<Collider2D>(), landed_on_left.GetComponent<Collider2D>()));
        
        Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), landed_on_left.GetComponent<Collider2D>(), true);
        Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), landed_on_right.GetComponent<Collider2D>(), true);
        StartCoroutine(IgnoreCollision(gameObject.GetComponent<Collider2D>(), landed_on_left.GetComponent<Collider2D>(), landed_on_right.GetComponent<Collider2D>()));
    }



    IEnumerator IgnoreCollision(Collider2D player,Collider2D c1, Collider2D c2)
    {
        yield return new WaitForSeconds(.5f);
        Physics2D.IgnoreCollision(player, c1, false);
        Physics2D.IgnoreCollision(player, c2, false);
        yield break;
    }

    public void DoubleJump()
    {
        if (CheckLanded() == true) { return; }
        if (double_jump_available == false) { return; }
        rgbd2d.AddForce(Vector2.up * 140f);
        double_jump_available = false;
        double_jump_animation.Play("Jump");
    }

    private void Movement()
    {
        if (Input.GetKey(KeyCode.D) && character.blinking <= character.control_cut_off_time && (attacking == false | onLader == true))
        {
            if (dust_ready == true && CheckLanded() == true )
            {
                if (transform.localScale.x == -1)
                {
                    dust_ready = false;
                    dust_animator.SpawnRunParticle(1f);
                    StartCoroutine(DustCD());
                }
            }
            transform.localScale = new Vector3(1, 1);
            if (CheckLanded() == true)
            {
                cape_animator.Run();
                rgbd2d.velocity = new Vector2(move_speed, rgbd2d.velocity.y);
                animator.SetFloat("Movement_Speed", move_speed);
            }
            else
            {
                if (onLader == true && crouched == false)
                {
                    rgbd2d.velocity = new Vector2(move_speed, rgbd2d.velocity.y);
                }
                else
                {
                    if (crouched == false)
                    {
                        rgbd2d.velocity = new Vector2(move_speed * air_control_rate, rgbd2d.velocity.y);
                    }
                }
            }
        }
        if (Input.GetKey(KeyCode.A) && character.blinking <= character.control_cut_off_time && (attacking == false | onLader == true))
        {
            if (dust_ready == true && CheckLanded() == true )
            {
                if (transform.localScale.x == 1)
                {
                    dust_ready = false;
                    dust_animator.SpawnRunParticle(-1f);
                    StartCoroutine(DustCD());
                }
            }
            transform.localScale = new Vector3(-1, 1);
            if (CheckLanded() == true)
            {
                cape_animator.Run();
                rgbd2d.velocity = new Vector2(-move_speed, rgbd2d.velocity.y);
                animator.SetFloat("Movement_Speed", move_speed);
            }
            else
            {
                if (onLader == true && crouched == false)
                {
                    rgbd2d.velocity = new Vector2(-move_speed, rgbd2d.velocity.y);
                }
                else {
                    if (crouched == false)
                    {
                        rgbd2d.velocity = new Vector2(-move_speed * air_control_rate, rgbd2d.velocity.y);
                    }
                }
            }
        }
    }

    bool onPlatform = false;
    Vector3 saved_position;
    GameObject saved_platform;

    void CheckMovingPlatform()
    {
        RaycastHit2D hit = Physics2D.Raycast(start_trigger.transform.position, -Vector2.up, distance, LayerMask.GetMask("Terrain"));
        RaycastHit2D hit2 = Physics2D.Raycast(start_trigger_2.transform.position, -Vector2.up, distance, LayerMask.GetMask("Terrain"));
        if (hit.transform == null && hit2.transform == null)
        {
            onPlatform = false;
            return;
        }
        if (onPlatform == false)//*TODO* dafuq i'm doing there????
        {
            bool landed = false;
            bool check = hit.transform != null;
            if (check)
            {
                landed = hit.transform.tag == "MovingPlatform" | hit.transform.tag == "SlavePlatform";
                saved_position = hit.transform.position;
                saved_platform = hit.transform.gameObject;
            }
            check = hit2.transform != null;
            if (check)
            {
                landed = landed | hit2.transform.tag == "MovingPlatform" | hit2.transform.tag == "SlavePlatform";
                saved_position = hit2.transform.position;
                saved_platform = hit2.transform.gameObject;
            }

            if (landed == true)
            {
                onPlatform = true;
            }
        }
        else {
            bool landed = false;
            Vector3 new_position = Vector3.zero ;
            bool check = hit.transform != null;
            if (check)
            {
                landed = hit.transform.tag == "MovingPlatform" | hit.transform.tag == "SlavePlatform";
                new_position = hit.transform.position;
            }
            check = hit2.transform != null;
            if (check)
            {
                landed = landed | hit2.transform.tag == "MovingPlatform" | hit2.transform.tag == "SlavePlatform";
                new_position = hit2.transform.position;
            }

            if (landed == true)
            {
                if (saved_platform.tag == "MovingPlatform" || saved_platform.tag == "SlavePlatform")
                {
                    transform.position -= saved_position - new_position;
                    saved_position = new_position;
                    if (saved_platform.tag == "SlavePlatform") { saved_platform.GetComponent<SlavePlatform>().Riding(gameObject); }
                }
            }
            else {
                onPlatform = false;
            }
        }
    }

    void ClimbUpLadder()
    {

        if (isClimbable == true)
        {
            if (onLader == false)
            {
                onLader = true;
                rgbd2d.gravityScale = 0f;
            }
            else
            {
                rgbd2d.velocity = new Vector2(rgbd2d.velocity.x, 1f);
                animator.SetBool("Climbing", true);
            }
        }
    }

    void ClimbDownLadder()
    {
        if (isClimbable == true)
        {
            if (onLader == false)
            {
                onLader = true;
                rgbd2d.gravityScale = 0f;
            }
            else
            {
                rgbd2d.velocity = new Vector2(rgbd2d.velocity.x, -1f);
                animator.SetBool("Climbing", true);
            }
        }
    }

    public void InLadderZone()
    {
        isClimbable = true;
        animator.SetBool("OnLadder", true);
        animator.Play("OnLadder", 0);
        cape_animator.Hide();
    }

    public void LeftLadderZone()
    {
        isClimbable = false;
        onLader = false;
        rgbd2d.gravityScale = default_gravity_scale;
        animator.SetBool("OnLadder", false);
        cape_animator.Show();
    }

    public bool attacking;

    void Attack()
    {
        if (onLader == true) { return; }
        if (attacking == true) { return; }
        attacking = true;
        if (crouched == false)
        {
            cape_animator.Attack();
        }
        if (Input.GetKey(KeyCode.W))
        {
            attacking = false;//*TODO* CRUNCH!!!
            attack_controller.ShootCurrentProjectile(360f);
            return;
        }
        if (Input.GetKey(KeyCode.S) && CheckLanded() == false && crouched != true)
        {
            attacking = false;//*TODO* CRUNCH!!!
            attack_controller.ShootCurrentProjectile(180f);
            return;
        }
        animator.SetTrigger("Attack_Direct");
        attack_controller.ShootCurrentProjectile(90f * transform.localScale.x);
    }

    bool dust_ready = true;
    public float dust_cd = 1f;
    IEnumerator DustCD()
    {
        yield return new WaitForSeconds(dust_cd);
        dust_ready = true;
        yield return null;
    }

    private void Jump()
    {
        if (pause_between_jumps > 0f)
        {
            pause_between_jumps -= Time.deltaTime;
        }

        if (pause_between_jumps <= 0f)
        {
            if (CheckLanded() == true)
            {
                if (jumping == true) { dust_animator.SpawnLandingParticle(); }

                jumping = false;
            }
        }

        animator.SetFloat("Movement_Speed", 0);//*TODO* optimize this
        if (Input.GetKeyDown(KeyCode.Space) && CheckLanded() == true && pause_between_jumps <= 0f && character.blinking <= character.control_cut_off_time)
        {
            //*TODO* jumping animation
            dust_animator.SpawnJumpParticle();
            jumping = true;
            pause_between_jumps = 0.3f;
            Vector2 v2 = rgbd2d.velocity;
            v2.y = 0f;
            rgbd2d.velocity = v2;
            rgbd2d.AddForce(Vector2.up * jump_force);
            jump_charger = 0.5f;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (jump_charger > 0f)
            {
                jump_charger -= Time.deltaTime;
                rgbd2d.AddForce(Vector2.up * jump_force_charger * Time.deltaTime);
            }
        }
    }

    private void StandUp()
    {
        crouched = false;
        animator.SetBool("Crouched", false);
    }

    private void CrouchControl()
    {
        if(character.blinking >= character.control_cut_off_time) { return; }
        crouched = false;
        if (Input.GetKey(KeyCode.S) && CheckLanded() == true )
        {
            crouched = true;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                DropDownThroughPlatform();
            }
        }
        if (fixed_crouched != crouched)
        {
            fixed_crouched = crouched;
            if (fixed_crouched == true)
            {
                //cape_animator.CrouchDown();
                Crouch();
            }
            else
            {
                cape_animator.StandUp();
                StandUp();
            }
        }
    }

    private void Crouch()
    {
        rgbd2d.velocity = new Vector2(0, rgbd2d.velocity.y);
        crouched = true;
        animator.SetBool("Crouched", true);
    }

    GameObject landed_on_left;
    GameObject landed_on_right;

    private bool CheckLanded()
    {
        bool landed = !crouched;

        RaycastHit2D hit = Physics2D.Raycast(start_trigger.transform.position, -Vector2.up, distance, LayerMask.GetMask("Terrain"));
        RaycastHit2D hit2 = Physics2D.Raycast(start_trigger_2.transform.position, -Vector2.up, distance, LayerMask.GetMask("Terrain"));
        if(hit.collider != null) { landed_on_left = hit.transform.gameObject; }
        if(hit2.collider != null) { landed_on_right = hit2.transform.gameObject; }
        if (hit.collider != null || hit2.collider != null)
        {
            animator.SetBool("InAir", false);
            double_jump_available = true;//that looks bad, i think i should move it somwhere else *TODO*
            return (landed & true);
        }

        animator.SetBool("InAir", true);
        return false;
    }

    public void FinishAttack()//*TODO* remove this???
    {
        attacking = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DdolScript : MonoBehaviour {

    public bool destroyable;

    private void Start()
    {
        DontDestroy();
    }
    
    public void DontDestroy()
    {
        GameObject[] gos = GameObject.FindGameObjectsWithTag("DontDestroy");
        if(gos.Length > 1)
        {
            for(int i = 0; i < gos.Length; i++)
            {
                if (gos[i].GetComponent<DdolScript>().destroyable == false)
                {
                    Destroy(gos[i]);
                }
            }
        }
        if (destroyable == false) {
            DontDestroyOnLoad(gameObject);
            destroyable = true;
        }
    } 
}

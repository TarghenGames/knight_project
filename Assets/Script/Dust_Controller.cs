﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dust_Controller : MonoBehaviour {

    public List<GameObject> dust_particle_list;

    private void Start()
    {
        //SpawnRunParticle();
    }

    public void SpawnJumpParticle()
    {
        Instantiate(dust_particle_list[1], transform.position, Quaternion.identity);
    }

    public void SpawnRunParticle(float direction)
    {
        Transform t = Instantiate(dust_particle_list[0], transform.position, Quaternion.identity).GetComponent< Transform>();
        Vector3 v3 = t.localScale;
        v3.x = direction;
        t.localScale = v3;
    }

    public void SpawnLandingParticle()
    {
        Instantiate(dust_particle_list[2], transform.position, Quaternion.identity);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlatformController : MonoBehaviour {

    bool onPlatform = false;
    public float distance = 0.3f;
    Vector3 saved_position;
    GameObject saved_platform;

    void Update ()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, distance, LayerMask.GetMask("Terrain"));
        if (hit.transform == null)
        {
            onPlatform = false;
            return;
        }
        if (onPlatform == false)
        {
            bool landed = false;
            bool check = hit.transform != null;
            if (check)
            {
                landed = hit.transform.tag == "MovingPlatform" | hit.transform.tag == "SlavePlatform";
                saved_position = hit.transform.position;
                saved_platform = hit.transform.gameObject;
            }

            if (landed == true)
            {
                onPlatform = true;
            }
        }
        else
        {
            bool landed = false;
            Vector3 new_position = Vector3.zero;
            bool check = hit.transform != null;
            if (check)
            {
                landed = hit.transform.tag == "MovingPlatform" | hit.transform.tag == "SlavePlatform";
                new_position = hit.transform.position;
            }

            if (landed == true)
            {
                if (saved_platform.tag == "MovingPlatform" || saved_platform.tag == "SlavePlatform")
                {
                    transform.position -= saved_position - new_position;
                    saved_position = new_position;
                    if (saved_platform.tag == "SlavePlatform") { saved_platform.GetComponent<SlavePlatform>().Riding(gameObject); }
                }
            }
            else
            {
                onPlatform = false;
            }
        }
    }

}

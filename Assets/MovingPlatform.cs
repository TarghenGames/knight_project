﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {
    public List<GameObject> waypoints;
    public float speed = 0.2f;
    public int current_waypoint = 0;
    int modifier = 1;

	// Update is called once per frame
	void Update () {
		if(waypoints == null) { return; }
        if(waypoints.Count == 0) { return; }
        transform.position = Vector3.MoveTowards(transform.position, waypoints[current_waypoint].transform.position, speed * Time.smoothDeltaTime);
        if (Vector3.Distance(transform.position, waypoints[current_waypoint].transform.position) < 0.0000005f)
        {
            
            if (current_waypoint >= waypoints.Count-1) {
                modifier = -1;
            }
            if (current_waypoint <= 0) {
                modifier = 1;
            }
            current_waypoint += modifier;
        }
    }
}

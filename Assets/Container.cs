﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour {
    [HideInInspector]
    public string knight_name;
    [HideInInspector]
    public bool restart = false;
    [HideInInspector]
    public int check_point = 0;

    public void SetCheckPoint(int waypoint_number)
    {
        if (check_point < waypoint_number)
            check_point = waypoint_number;
    }

    public void Null()
    {
        Debug.Log("Nulling container");
        knight_name = "";
        restart = false;
        check_point = 0;
    }
}

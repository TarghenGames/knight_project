﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropPlatform : MonoBehaviour {

    bool timed = false;
    bool fall = false;
    public float timer = 3f;
    float shaker = 0.05f;
    float direction = 1f;

    private void Update()
    {
        if (timed == true)
        {
            timer -= Time.deltaTime;
            shaker -= Time.deltaTime;
            if (shaker < 0f)
            {
                transform.position += Vector3.right * 0.01f * direction;
                direction = direction * (-1);
                shaker = 0.1f;
            }
            if (timer < 0f)
            {
                timed = false;
                fall = true;
            }
        }
        if (fall == true)
        {
            transform.position += Vector3.down * 2f * Time.deltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player") {
            if (timer > 0f)
            {
                timed = true;
            }
        }
    }
}

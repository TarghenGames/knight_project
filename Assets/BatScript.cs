﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatScript : MonoBehaviour {

    [HideInInspector]
    public Vector3 target_position;
    public int bite_damage = 1;
    Vector3 nest;

    bool move;
    bool coming_back;
    bool predating;

    GameObject player;

    float speed = 0.5f;
    bool mediating = false;

    //better to do trhough delegate
    HungryCharacter hungry;

    private void Start()
    {
        player = GameObject.Find("Player");
        nest = transform.position;
        target_position = nest;
        move = false;
        coming_back = false;
        predating = false;
        hungry = gameObject.GetComponent<HungryCharacter>();
    }

    private void Update()
    {
        if (hungry != null && move == false && coming_back == false)
        {
            if (hungry.food != null)
            {
                target_position = hungry.food.transform.position;
                move = true;
                GetComponent<Animator>().SetBool("Attack", true);
                predating = true;
            }
        }
        if (Vector3.Distance(player.transform.position, nest) < 1f && move == false && coming_back == false)
        {
            target_position = player.transform.position;
            move = true;
            GetComponent<Animator>().SetBool("Attack", true);
        }
        if (move == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, target_position, speed * Time.deltaTime);

            CheckFinish();
        }
        if (coming_back == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, nest, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, nest) < 0.05f)
            {
                coming_back = false;
                GetComponent<Animator>().SetBool("Attack", false);
            }
        }
        /*
        if (mediating == false)
        {
            if (speed > 0.2f)
            {
                speed -= 0.05f;
            }
        }
        else 
        {
            if (speed < 0.8f)
            {
                speed += 0.05f;
            }
        }*/
    }

    void CheckFinish()//*TODO* i don't like this
    {
        if (Vector3.Distance(transform.position, target_position) < 0.06f)
        {
            if (predating == true)
            {
                if (Vector3.Distance(hungry.food.transform.position, transform.position) < 0.06f)
                {
                    hungry.food.GetComponent<Bunny>().Eaten();
                    predating = false;
                }
                else
                {
                    if (Vector3.Distance(hungry.food.transform.position, transform.position) < 1f)
                    {
                        target_position = hungry.food.transform.position;
                    }
                }
            }
            else
            {
                if (Vector3.Distance(player.transform.position, transform.position) < 0.6f)
                {
                    target_position = player.transform.position;
                }
                else
                {
                    move = false;
                    coming_back = true;
                    predating = false;
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            bool f = false;
            if (collision.transform.position.x > gameObject.transform.position.x)
            {
                f = false;
            }
            else
            {
                f = true;
            }
            collision.gameObject.GetComponent<PlayerCharacter>().TakeDamage(bite_damage, f);
            if (move == true)
            {
                move = false;
                coming_back = true;
                predating = false;
            }
        }
    }

    public void Stop()
    {
        this.enabled = false;
    }

    public void MediateMovementSpeed()
    {
        mediating = !mediating;
    }
}

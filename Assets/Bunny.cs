﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bunny : MonoBehaviour {
    public Sprite bunny_idle;

    GameObject player;

    bool runing;
    bool landed;
    float timer;
    //float jump_timer; deprecated by Unkab request he want bunny to run and not hop

    Rigidbody2D rgbd2d;

    private void Start()
    {
        transform.rotation = Quaternion.identity;
        player = GameObject.Find("Player");
        timer = UnityEngine.Random.Range(2f, 6f);
        //jump_timer = 1f;
        runing = false;
        landed = false;
        rgbd2d = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (runing == true)
        {//*TODO* optimize spaghetti
            timer -= Time.deltaTime;
            /*jump_timer -= Time.deltaTime;
            if (jump_timer <= 0)
            {
                ApplyForce(new Vector2(0.5f * transform.localScale.x , 0.5f));
                jump_timer = 1f;
            }*/

            rgbd2d.velocity = new Vector2(0.5f * transform.localScale.x, rgbd2d.velocity.y);
            
            if (Vector3.Distance(transform.position, player.transform.position) < 0.3f)
            {
                runing = true;
                timer = UnityEngine.Random.Range(2f, 6f);
                //Rotate();
                if (transform.position.x > player.transform.position.x) { transform.localScale = new Vector3(1, 1, 1); }
                else { transform.localScale = new Vector3(-1, 1, 1); }
            }

            if (timer <= 0f)
            {
                timer = UnityEngine.Random.Range(2f, 6f);
                runing = false;
                GetComponent<Animator>().enabled = false;
                GetComponent<SpriteRenderer>().sprite = bunny_idle;
            }
        }
        else {
            if (landed == false) { return; }
            timer -= Time.deltaTime;
            if (Vector3.Distance(transform.position, player.transform.position) < 0.3f)
            {
                GetComponent<Animator>().enabled = true;
                GetComponent<Animator>().Play("Run");
                //GetComponent<SpriteRenderer>().sprite = bunny_idle;
                runing = true;
                timer = UnityEngine.Random.Range(2f, 6f);
                //Rotate();
                if (transform.position.x > player.transform.position.x) { transform.localScale = new Vector3(1, 1, 1); }
                else { transform.localScale = new Vector3(-1, 1, 1); }
            }
            if (timer <= 0f)
            {
                runing = true;
                GetComponent<Animator>().enabled = true;
                GetComponent<Animator>().Play("Run");
                timer = UnityEngine.Random.Range(2f, 6f);
                Rotate();
            }
            
        }
    }

    private void Rotate()
    {
        if (UnityEngine.Random.value > 0.5f)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (landed == true) { return; }
        GetComponent<Animator>().enabled = false;
        GetComponent<SpriteRenderer>().sprite = bunny_idle;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        transform.rotation = Quaternion.identity;
        landed = true;
    }

    public void ApplyForce(Vector2 directionOfVelocity)
    {
        GetComponent<Rigidbody2D>().AddForce(directionOfVelocity);
    }

    public void Eaten()
    {
        //*TODO* animate it
        Destroy(gameObject);
    }
}

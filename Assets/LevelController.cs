﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
    public string level_name;
    public List<GameObject> check_point;
    Container player_info_container;

    private void Start()
    {
        player_info_container = GameObject.Find("Container").GetComponent<Container>();
        if (player_info_container.restart == true)
        {
            Debug.Log("Loading check point position");
            GameObject.Find("Player").transform.position = check_point[player_info_container.check_point].transform.position;
        }
    }
}

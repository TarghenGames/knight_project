﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        //*TODO* fix this, move to OnTrigger

        GameObject player = GameObject.Find("Player");
        if (player != null)
        {
            GameObject.Find("Player").GetComponent<PlayerCharacter>().AddCoin();
        }
    }
}

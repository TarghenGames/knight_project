﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

    public int damage;
    float ttl = 3f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            collision.GetComponent<EnemyCharacter>().HitByAttack(damage);
        }
    }

    private void Update()
    {
        ttl -= Time.deltaTime;
        if (ttl < 0f) { Destroy(gameObject); }
    }
}

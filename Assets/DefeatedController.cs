﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DefeatedController : MonoBehaviour {

    public void Continue()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(Camera.main.GetComponent<LevelController>().level_name);
        GameObject.Find("Container").GetComponent<Container>().restart = true;//set restart mark so it will load from checkpoint
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        GameObject.Find("Container").GetComponent<Container>().check_point = 0;
        SceneManager.LoadScene(Camera.main.GetComponent<LevelController>().level_name);
    }

    public void Exit()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    public void Unpause()
    {
        Time.timeScale = 1f;
        BlackScreenController.black_screen.HideBlackScreen();
    }
}

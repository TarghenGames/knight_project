﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivatorController : MonoBehaviour {
    GameObject pressEPanel;

    GameObject player;

    GameObject current_event;
    DialoguePanelController text;

	// Use this for initialization
	void Start () {
        pressEPanel = GameObject.Find("Press E Panel");
        pressEPanel.SetActive(false);
        text = GameObject.Find("Dialogue").GetComponent<DialoguePanelController>();
        text.gameObject.SetActive(false);
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (pressEPanel.activeInHierarchy == true)
            {
                text.StartDialogue(current_event);
            }
        }

        if (current_event != null)
        {
            Vector2 pos = current_event.transform.position;  // get the game object position
            Vector2 viewportPoint = Camera.main.WorldToViewportPoint(pos);  //convert game object position to VievportPoint

            // set MIN and MAX Anchor values(positions) to the same position (ViewportPoint)
            text.rectTransform.anchorMin = viewportPoint;
            text.rectTransform.anchorMax = viewportPoint;
            //adjusting position of text window according player position
            text.rectTransform.anchoredPosition = Vector2.up * 100f;
            if (player.transform.position.x < current_event.transform.position.x)
            {
                text.rectTransform.anchoredPosition += Vector2.right * 60f;
            }
            else
            {
                text.rectTransform.anchoredPosition += Vector2.left * 60f;
            }
        }
    }

    public void Null(GameObject gameObject)
    {
        if (current_event == gameObject)
        {
            current_event = null;
            pressEPanel.SetActive(false);
            text.Hide();
        }
    }

    public void SetEvent(GameObject activator)
    {
        current_event = activator;

        pressEPanel.SetActive(true);
        Camera.main.GetComponent<PointerController>().HoverOver(activator);
    }
}

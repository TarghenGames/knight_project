﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlock : MonoBehaviour {
    public float ttl = 4f;
    float current_ttl;

	// Use this for initialization
	void Start () {
        current_ttl = ttl;
    }
	
	// Update is called once per frame
	void Update () {
        current_ttl -= Time.deltaTime;
        if (current_ttl < 0f)
        {
            gameObject.GetComponent<Animator>().Play("Destroy");
        }
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }

    public void DisableCollider()
    {
        gameObject.GetComponent<Collider2D>();
    }
}

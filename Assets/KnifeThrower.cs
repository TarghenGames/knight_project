﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeThrower : MonoBehaviour {

    public float period_between_attacks = 2f;
    public GameObject knife;
    GameObject player;

    float timer = 0f;
    Animator animator;

    EnemyCharacter enemy_character;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        player = GameObject.Find("Player");
        enemy_character = GetComponent<EnemyCharacter>();
    }
	
	// Update is called once per frame
	void Update () {
        if (enemy_character.hp <= 0) { return; }
        if (timer > 0f)
        {
            timer -= Time.deltaTime;
        }
        else {
            
            animator.SetTrigger("Attack");
            timer = period_between_attacks;
            ThrowKnife();
        }

        if (player.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector3(1, 1);
        }
        else {
            transform.localScale = new Vector3(-1, 1);
        }
	}

    void ThrowKnife()
    {
        GameObject go = Instantiate(knife, transform.position + (Vector3.left * 0.1f * transform.localScale.x), Quaternion.identity);
        go.GetComponent<Rigidbody2D>().velocity = new Vector2(-1.5f * transform.localScale.x, 0);
        go.transform.localScale = new Vector3(transform.localScale.x, 1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerController : MonoBehaviour {

    public GameObject pointer;

    public void Show()
    {
        pointer.SetActive(true);
    }

    public void Hide()
    {
        pointer.SetActive(false);
    }

    public void HoverOver(GameObject go)
    {
        Vector2 v2 = new Vector2(go.transform.position.x, go.GetComponent<Collider2D>().bounds.max.y);
        pointer.transform.position = v2;
    }
}

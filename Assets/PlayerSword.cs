﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSword : MonoBehaviour {
    float ttl = 0.5f;
    public List<Sprite> sprites_swords;
    SpriteRenderer sprite_renderer;

    private void Start()
    {
        sprite_renderer = GetComponent<SpriteRenderer>();
        sprite_renderer.sprite = sprites_swords[Random.Range(0, sprites_swords.Count)];
    }

    // Update is called once per frame
    void Update () {
        ttl -= Time.deltaTime;
        if (ttl < 0.2f)
        {
            sprite_renderer.color = new Color(1f, 1f, 1f, 0.5f);
        }
        if (ttl < 0f) {
            Destroy(gameObject);
        }
	}
}

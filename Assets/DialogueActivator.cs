﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueActivator : MonoBehaviour {

    public string text_to_talk;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<ActivatorController>().SetEvent(gameObject);
            Camera.main.GetComponent<PointerController>().Show();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<ActivatorController>().Null(gameObject);
            Camera.main.GetComponent<PointerController>().Hide();
        }
    }
}

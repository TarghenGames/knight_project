﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartButton : MonoBehaviour {

    public GameObject name_InputField;

    public void StartTheGame()
    {
        Container cnt = GameObject.Find("Container").GetComponent<Container>();
        cnt.Null();
        cnt.knight_name = name_InputField.GetComponent<InputField>().text;

        if (cnt.knight_name != "")
        {
            SceneManager.LoadScene("Movement");
        }
    }
}

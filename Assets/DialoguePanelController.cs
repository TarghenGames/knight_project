﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialoguePanelController : MonoBehaviour {
    Text text;
    string to_print;
    string current_string;
    int case_id;
    float timer;
    public float case_frequency = 0.1f;
    public RectTransform rectTransform;

    private void OnEnable()
    {
        text = transform.GetChild(0).GetComponent<Text>();
        rectTransform = gameObject.GetComponent<RectTransform>();
    }

    public void StartDialogue(GameObject activator)
    {
        gameObject.SetActive(true);
        to_print = activator.GetComponent<DialogueActivator>().text_to_talk;
        case_id = 0;
        current_string = "";
        if(text == null) { text = transform.GetChild(0).GetComponent<Text>(); }
        text.text = "";
    }

    private void Update()
    {
        if (current_string.Length == to_print.Length) { return; }
        if (timer < 0f) {
            current_string += to_print[case_id];
            timer = case_frequency;
            case_id += 1;
            text.text = current_string;
        }
        timer -= Time.deltaTime;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        case_id = 0;
        current_string = "";
        //text.text = "";
        to_print = "";
    }
}

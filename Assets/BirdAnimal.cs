﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdAnimal : MonoBehaviour {

    GameObject player;
    bool runing_away;
    Vector3 runaway_position;
    float speed = 1.5f;

    private void Start()
    {
        player = GameObject.Find("Player");
        runing_away = false;

        runaway_position = transform.position + (Vector3.up * 50) + (Vector3.right * Random.Range(-30,30));
        if (runaway_position.x > transform.position.x) { transform.localScale = new Vector3(1, 1, 1); }
        else { transform.localScale = new Vector3(-1, 1, 1); }
    }

    // Update is called once per frame
    void Update ()
    {
        if (runing_away == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, runaway_position, speed * Time.deltaTime);
        }
        else {
            if (Vector3.Distance(player.transform.position, transform.position) < 0.5f)
            {
                runing_away = true;
                gameObject.GetComponent<Animator>().enabled = true;
            }
        }
	}
}

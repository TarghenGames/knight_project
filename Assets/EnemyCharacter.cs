﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : MonoBehaviour {
    public int hp = 1;
    public int score_reward = 100;

    public void HitByAttack(int dmg)
    {
        hp -= dmg;
        if (hp <= 0)
        {
            GetComponent<Animator>().SetBool("Dead", true);
            GetComponent<Collider2D>().enabled = false;
            if (GetComponent<Rigidbody2D>())
            {
                GetComponent<Rigidbody2D>().simulated = false;
            }
            if (GetComponent<BatScript>())
            {
                GetComponent<BatScript>().Stop();
            }
        }
        else {
            GetComponent<BlinkController>().Blink();
        }
    }

    public void DestroyMe()
    {
        GameObject.Find("Player").GetComponent<PlayerCharacter>().SCORE += score_reward;
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float bacgkroundSize;
    public float paralaxSpeed;
    public float autoScrolingSpeed;

    private Transform cameraTransform;
    private Transform[] layers;
    private float viewZone = 5;
    private int leftIndex;
    private int rightIndex;
    float lastCameraX;
    

    private void Start()
    {
        cameraTransform = Camera.main.transform;
        layers = new Transform[transform.childCount];
        lastCameraX = cameraTransform.position.x;
        for (int i = 0; i < transform.childCount; i++)
        {
            layers[i] = transform.GetChild(i);
        }

        leftIndex = 0;
        rightIndex = layers.Length - 1;
    }

    private void Update()
    {
        float deltaX = cameraTransform.position.x - lastCameraX;
        transform.position += Vector3.right * (deltaX * paralaxSpeed);
        transform.position += Vector3.right * autoScrolingSpeed;
        lastCameraX = cameraTransform.position.x;

        if (cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone))
        {
            ScrollLeft();
        }

        if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone))
        {
            ScrollRight();
        }
    }

    private void ScrollLeft()
    {
        int lastRight = rightIndex;
        layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - bacgkroundSize) + Vector3.up * layers[rightIndex].position.y;
        leftIndex = rightIndex;
        rightIndex--;
        if (rightIndex < 0) { rightIndex = layers.Length - 1; }
    }

    private void ScrollRight()
    {
        int lastLeft = leftIndex;
        layers[leftIndex].position = Vector3.right * (layers[rightIndex].position.x + bacgkroundSize) + Vector3.up * layers[rightIndex].position.y;
        rightIndex = leftIndex;
        leftIndex++;
        if (leftIndex >= layers.Length) { leftIndex = 0; }
    }
}

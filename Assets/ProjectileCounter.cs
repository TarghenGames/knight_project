﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCounter : MonoBehaviour {
    public GameObject source;

    private void OnDestroy()
    {
        if (source != null)
        {
            source.GetComponent<PlayerWeaponController>().RemoveMeFromList(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HungryCharacter : MonoBehaviour {
    public GameObject food;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Animal")
        {
            food = collision.gameObject;
            gameObject.GetComponent<BatScript>().target_position = food.transform.position;
        }
    }
}

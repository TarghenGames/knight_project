﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RageCloud : MonoBehaviour {

    public float speed = 0.7f;

    float curent_position_beacon;

    Vector3 current_position;
    Vector3 start_position;

    GameObject player;

    public GameObject beacon;
    RageCloudBeacon beacon_rage;

    public GameObject l_bolt;

    bool attacking;
    float in_between_lightning;

    private void Start()
    {
        start_position = transform.position;
        curent_position_beacon = 0f;
        player = GameObject.Find("Player");
        beacon_rage = beacon.GetComponent<RageCloudBeacon>();
        attacking = false;
    }

    private void OnDestroy()
    {
        Destroy(transform.parent.gameObject);
    }

    private void Update()
    {
        if (in_between_lightning >= 0f) { in_between_lightning -= Time.deltaTime; }
        Move();
        if (beacon_rage.current_target == null) { return; }
        if (Vector3.Distance(transform.position, beacon_rage.current_target.transform.position) < 0.5f)
        {
            Attack();
        }

    }

    private void Move()
    {
        if (beacon_rage.current_target == null) { return; }
        transform.position = Vector3.MoveTowards(transform.position,
            beacon_rage.current_target.transform.position + (Vector3.up * 0.4f), speed * Time.deltaTime);
    }

    private void Attack()
    {
        if (in_between_lightning <= 0f)
        {
            attacking = true;
            in_between_lightning = 3f;
            GetComponent<Animator>().Play("Attack");
        }
    }

    public void ShootLightning()
    {
        GameObject go = Instantiate(l_bolt, transform.position, Quaternion.identity);
        go.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1f);
    }
}

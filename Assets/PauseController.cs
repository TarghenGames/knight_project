﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

    GameObject player;

    private void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (player.GetComponent<PlayerCharacter>().IS_ALIVE == true)
            {
                if (Time.timeScale == 1f)
                {
                    Time.timeScale = 0f;
                    BlackScreenController.black_screen.ShowBlackScreen();
                    BlackScreenController.black_screen.SetType(BlackScreenController.BlackScreenEnum.Menu);
                }
                else
                {
                    if (BlackScreenController.black_screen.to_show == BlackScreenController.BlackScreenEnum.Death) { return; }
                    Time.timeScale = 1f;
                    BlackScreenController.black_screen.HideBlackScreen();
                }
            }
        }
	}
}

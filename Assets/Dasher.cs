﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dasher : MonoBehaviour {
    Rigidbody2D rgdbd2d;
    float direction = 1;
    float time;
    float speed = 0.9f;

    public int damage = 1;

    private void Start()
    {
        rgdbd2d = GetComponent<Rigidbody2D>();
    }

    void Update ()
    {
        time -= Time.deltaTime;
        if (time < 0f)
        {
            direction *= -1;
            time = Random.Range(2, 6);
            transform.localScale = new Vector3(-direction, 1, 1);
        }
        rgdbd2d.velocity = new Vector2(speed * direction, rgdbd2d.velocity.y);

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            bool f = false;
            if (collision.transform.position.x > gameObject.transform.position.x)
            {
                f = false;
            }
            else
            {
                f = true;
            }
            collision.gameObject.GetComponent<PlayerCharacter>().TakeDamage(damage, f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlavePlatform : MonoBehaviour {

    public GameObject max_left_wp;
    public GameObject max_right_wp;
    public GameObject rider;
    public float speed = 0.15f;

    public void Riding(GameObject rider)
    {
        if (max_left_wp == null || max_right_wp == null) { return; }
        if (rider.transform.position.x > transform.position.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, max_right_wp.transform.position, speed * Time.smoothDeltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, max_left_wp.transform.position, speed * Time.smoothDeltaTime);
        }
    }
}

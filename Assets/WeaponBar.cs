﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponBar : MonoBehaviour {

    public List<Sprite> list;

    internal void UpdateSelectedWeapon(int selected_projectile)
    {
        GetComponent<Image>().sprite = list[selected_projectile];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableBlock : MonoBehaviour {

    int life;

    private void Start()
    {
        life = 1;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Projectile")
        {
            life -= 1;
            if (life <= 0)
            {
                gameObject.GetComponent<Collider2D>().enabled = false;
                gameObject.GetComponent<Animator>().Play("Drible");
            }
        }
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }

}

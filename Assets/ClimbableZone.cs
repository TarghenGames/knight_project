﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbableZone : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            other.GetComponent<PlayerControllScript>().InLadderZone();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            other.GetComponent<PlayerControllScript>().LeftLadderZone();
        }
    }
}

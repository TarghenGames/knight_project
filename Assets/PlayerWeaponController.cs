﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponController : MonoBehaviour {
    public List<GameObject> projectile;
    int selected_projectile = 1;
    List<GameObject> existing_projectile;
    int max_projectile_count = 2;

    int SELECTED_PROJECTILE {
        get { return selected_projectile; }
        set {
            selected_projectile = value;
            if (selected_projectile  >= projectile.Count) { selected_projectile = 0; }
            if (selected_projectile < 0) { selected_projectile = projectile.Count-1; }
            weapon_bar_body.UpdateSelectedWeapon(selected_projectile);
        }
    }

    WeaponBar weapon_bar_body;

    GameObject up;
    GameObject down;

    private void Start()
    {
        up = transform.GetChild(0).gameObject;
        down = transform.GetChild(1).gameObject;
        weapon_bar_body = GameObject.Find("weapon_bar").GetComponent<WeaponBar>();
        SELECTED_PROJECTILE = 0;
        existing_projectile = new List<GameObject>();
    }

    public void ShootCurrentProjectile(float angle)//*TODO* add velocity
    {
        if (existing_projectile.Count >= max_projectile_count) { return; }
        //if (vector > 0) { vector = 1f; } else { vector = -1f; }
        GameObject go = Instantiate(projectile[selected_projectile], transform.position, Quaternion.identity);
        go.transform.Rotate(-Vector3.forward * angle);
        
        ProjectileCounter pc = go.GetComponent<ProjectileCounter>();
        if (pc != null) {
            pc.source = gameObject;
            existing_projectile.Add(go);
        }

        //go.GetComponent<ProjectileScript>().damage = 2;
        Vector2 directionOfVelocity = new Vector2();
        if (angle == 90f) {
            directionOfVelocity = new Vector2(2f, 0);
        }
        if (angle == 180f) {
            directionOfVelocity = new Vector2(0, -2f);
            go.transform.position = down.transform.position;
            //fire sword activation event, I don't like where it placed maybe i should redo it in fire sword script *TODO*
            if (selected_projectile == 3) {
                transform.parent.GetComponent<PlayerControllScript>().DoubleJump();
            }
        }
        if (angle == 270f || angle == -90f)
        {
            directionOfVelocity = new Vector2(-2f, 0);
        }
        if (angle == 360f || angle == 0f)
        {
            directionOfVelocity = new Vector2(0, 2f);
            go.transform.position = up.transform.position;
        }

        if (selected_projectile == 1)
        {
            go.GetComponent<Bunny>().ApplyForce(directionOfVelocity/2 + Vector2.up*1f);
        }
        else {
            go.GetComponent<Rigidbody2D>().velocity = directionOfVelocity;
        }
    }

    public void RemoveMeFromList(GameObject projectile)
    {
        existing_projectile.Remove(projectile);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SELECTED_PROJECTILE -= 1;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            SELECTED_PROJECTILE += 1;
        }
    }

}

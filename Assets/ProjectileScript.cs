﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {
    public int damage;
    public bool enemy;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (enemy == true)
        {
            if (coll.gameObject.tag == "Player")
            {
                bool f = false;
                if (coll.transform.position.x > gameObject.transform.position.x)
                {
                    f = false;
                }
                else {
                    f = true;
                }
                coll.transform.GetComponent<PlayerCharacter>().TakeDamage(damage, f);
            }
        }
        else
        {
            if (coll.gameObject.tag == "Enemy")
            {
                coll.transform.GetComponent<EnemyCharacter>().HitByAttack(damage);
            }
        }
        Destroy(gameObject);
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkController : MonoBehaviour {
    Material default_material;
    public Material white_blanker;

	// Use this for initialization
	void Start () {
        default_material = GetComponent<SpriteRenderer>().material;

    }

    public void Blink()
    {
        StopAllCoroutines();
        GetComponent<SpriteRenderer>().material = white_blanker;
        StartCoroutine(BlinkCoroutine());
    }

    IEnumerator BlinkCoroutine()
    {
        yield return new WaitForSeconds(.15f);
        GetComponent<SpriteRenderer>().material = default_material;
        yield break;
    }
}

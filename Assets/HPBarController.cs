﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarController : MonoBehaviour {

    List<GameObject> hearts;

    public Sprite heart_empty;
    public Sprite healt_half;
    public Sprite heart_full;

    public void Initialize(int max_hp, int current_hp)
    {
        hearts = new List<GameObject>();
        bool half_heart = true;
        while (hearts.Count < transform.childCount) {
            hearts.Add(transform.GetChild(hearts.Count).gameObject);
        }
        for (int i = 0; i < hearts.Count; i++) {
            if (i < max_hp / 2)
            {
                hearts[i].SetActive(true);
            }
            else {
                if (half_heart == true)
                {
                    hearts[i].SetActive(false);
                    half_heart = false;
                    if (max_hp % 2 > 0)
                    {
                        hearts[i].SetActive(true);
                    }
                }
                else {
                    hearts[i].SetActive(false);
                }
            }
        }

        HPBarUpdate(current_hp);
    }

    public void HPBarUpdate(int current_hp)
    {
        bool half_heart = true;
        for (int i = 0; i < hearts.Count; i++)
        {
            if (i < current_hp / 2)
            {
                hearts[i].GetComponent<Image>().sprite = heart_full;
            }
            else
            {
                if (half_heart == true)
                {
                    hearts[i].GetComponent<Image>().sprite = heart_empty;
                    half_heart = false;
                    if (current_hp % 2 > 0)
                    {
                        hearts[i].GetComponent<Image>().sprite = healt_half;
                    }
                }
                else
                {
                    hearts[i].GetComponent<Image>().sprite = heart_empty;
                }
            }
        }
    }
}

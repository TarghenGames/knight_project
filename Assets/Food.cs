﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour {

    public enum FoodType
    {
        Apple,
        IceCream,
        Meat
    }

    public FoodType food_type;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<PlayerCharacter>().Heal(GetHealAmount());
            Destroy(gameObject);
        }
    }

    public int GetHealAmount()
    {
        switch (food_type)
        {
            case FoodType.Apple:
                return 2;
            case FoodType.Meat:
                return 4;
            case FoodType.IceCream:
                return 6;
        }
        return 1;
    }
}

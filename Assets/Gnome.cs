﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gnome : MonoBehaviour {

    GameObject player;
    bool runing;
    public float reaction_distance = 1f;
    public float damage_distance = 0.2f;
    Rigidbody2D rgbd2d;
    public int damage = 1;

    private void Start()
    {
        player = GameObject.Find("Player");
        rgbd2d = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update () {
        if (runing == false)
        {
            if (Vector3.Distance(transform.position, player.transform.position) < reaction_distance)
            {
                GetComponent<Animator>().SetBool("Run", true);
                runing = true;
                if (transform.position.x > player.transform.position.x) { transform.localScale = new Vector3(1, 1, 1); }
                else { transform.localScale = new Vector3(-1, 1, 1); }
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, player.transform.position) > reaction_distance)
            {
                runing = false;
                GetComponent<Animator>().SetBool("Run", false);
            }
            else {
                rgbd2d.velocity = new Vector2(-(0.7f * transform.localScale.x), rgbd2d.velocity.y);
                if (transform.position.x > player.transform.position.x) { transform.localScale = new Vector3(1, 1, 1); }
                else { transform.localScale = new Vector3(-1, 1, 1); }
            }
        }
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            bool f = false;
            if (collision.transform.position.x > gameObject.transform.position.x)
            {
                f = false;
            }
            else
            {
                f = true;
            }
            collision.gameObject.GetComponent<PlayerCharacter>().TakeDamage(damage, f);
        }
    }
}
